#!/bin/bash
#
# aws_image_bkup.sh : awscliでAMIバックアップする
# 依存関係：awsコマンド
# 更新履歴：20150518 – create ishii
#
# 引数に従ってインスタンスのAMIをcreate
# 戻り値のimageidをファイルに出力してそれを用いてタグをつける
# このときRootDeviceNameのsnapshotかどうかでタグがosと判断しデバイス名もタグ名に含める
# 戻り値のimageidでホスト名のフィルタで検索してホストにかかわる全てのimageidを得て日時のフィールドで降順にソート
# 残したい世代数の行分を削除して消す対象の古いimageidを得て、deregistとsnapshot消去する
# 世代数はbkup_numという変数に定義

## aws-cliのパス
export PATH=$PATH:/usr/bin
## configのパス
export AWS_CONFIG_FILE=/home/ec2-user/.aws/aws.config

## 引数のチェック
if [ $# -ne 2 ];then
    echo "usage: $0 [instance-id] [hostname]"
    exit 1
else
    myInstanceID=$1
    host_name=$2
fi

## 変数のセット
base=`dirname $0`
LOG=${base}/bkup.log
CurrentImage_LOG=${base}/cimageid_${myInstanceID}.log
today=`/bin/date +%Y%m%d`
amitime=`/bin/date +%Y%m%d_%H%M`
datetime=`/bin/date +%Y/%m/%d_%H:%M:%S`
mail_to="yishii0117@gmail.com"
bkup_num=2

## functions
mail_send(){
    echo ${mail_body}|tee -a ${LOG}|mail -s ${mail_title} ${mail_to}
}
create_ami(){
    aws ec2 create-image --instance-id ${myInstanceID} --name "${host_name}_${amitime}" \
        --description "${host_name}_${myInstanceID}_${datetime}" --no-reboot | tee ${CurrentImage_LOG}
    grep ImageId ${CurrentImage_LOG}
    res_create=$?
}
deregister_ami(){
    if [ -z ${OLDimageID} ];then
        echo "this is first ami" >> ${LOG}
    else
        aws ec2 deregister-image --image-id ${OLDimageID}
        echo "deregister ami ${OLDimageID}" >> ${LOG}
    fi
}
delete_snapshot(){
    if [ -z ${OLDimageID} ];then
        echo "this is first snapshot" >> ${LOG}
    else
        snapshot_ids=`aws ec2 describe-snapshots --filters Name=description,Values=*${OLDimageID}* \
            |grep SnapshotId|awk -F : '{print $2}'|sed -e 's/[ ",]//g'`
        for i in ${snapshot_ids}
        do
            aws ec2 delete-snapshot --snapshot-id ${i}
            echo "delete snapshot ${i}" >> ${LOG}
        done
    fi
}
create_tag(){
    ### create tag for ami
    CurImageID=`cat ${CurrentImage_LOG}|grep ImageId|awk '{FS=":";print $2}'|sed -e 's/[ "]//g'`
    aws ec2 create-tags --resources ${CurImageID} --tags "Key=Name,Value=${host_name}_${today}"
    ### create tags for snapshots
    rootDeviceName=`aws ec2 describe-instances --instance-ids ${myInstanceID} \
        |grep RootDeviceName|awk '{print $2}'|sed -e 's/[",]//g'`
    snapshot_ids=`aws ec2 describe-snapshots --filters Name=description,Values=*${CurImageID}* \
        |grep SnapshotId|awk -F : '{print $2}'|sed -e 's/[ ",]//g'`
    for i in ${snapshot_ids}
    do
        volume_id=`aws ec2 describe-snapshots --snapshot-ids ${i}|grep VolumeId \
            |awk '{print $2}'|sed -e 's/[",]//g'`
        deviceName=`aws ec2 describe-volumes --volume-ids ${volume_id}|grep Device \
            |awk '{print $2}'|sed -e 's/"//g'`
        if [ "${rootDeviceName}" == "${deviceName}" ];then
            aws ec2 create-tags --resources ${i} \
                --tags "Key=Name,Value=AMI_${host_name}_${today}-os_${deviceName}"
        else
            aws ec2 create-tags --resources ${i} \
                --tags "Key=Name,Value=AMI_${host_name}_${today}-data_${deviceName}"
        fi
    done
}

## main
exec >> ${LOG} # 標準出力をログに出力（追記）
exec 2>&1 # 標準エラーを標準出力と同じ宛先に
echo "start ami backup. ${datetime}"
create_ami
## rotate_ami
delete_images=`aws ec2 describe-images --filters Name=name,Values=*${host_name}*\
    |egrep -a 'ImageId|ImageLocation'|sed -e "N;s/\n//" -e 's/[",]//g'|awk '{print $2" "$4}'\
    |sort -r -t _ -k 2,3|sed -e "1,${bkup_num}d"|awk '{print $1}'`
for i in ${delete_images}
do
    OLDimageID=${i}
    deregister_ami
    delete_snapshot
done
## AMI作成エラーが出たらアラートメール送信、そうでなければAMIとsnapshotにtag付け
if [ ${res_create} -ne 0 ];then
    mail_body="NG create ami for ${myInstanceID}(${host_name}) at ${datetime}"
    mail_title="NG_create_ami_${today}"
    mail_send
elif [ ${res_create} -eq 0 ];then
    create_tag
    mail_body="OK create ami for ${myInstanceID}(${host_name}) at ${datetime}"
    mail_title="OK_create_ami_${today}"
    mail_send
fi
echo "end ami backup. ${datetime}"

## logrotate yearly
DAY=`date +%m%d`
if [ $DAY = "0101" ]
then
    OY=`date -d '1 year ago' +%Y`
    mv $LOG $LOG.$OY
fi
exit 0
