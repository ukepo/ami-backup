# How to use

```
chmod +x aws_image_bkup.sh
./aws_image_bkup.sh [instance-id] [hostname]
```

# Multiple instances

```
vi bkup-hosts.txt
—-
i-xxxxx,hoge-web01
i-yyyyy,hoge-web02
…
—-
for i in cat bkup-hosts.txt
do
  instance-id=echo $i|awk -F, '{print $1}'
  host-name=echo $i|awk -F, '{print $2}'
  ./aws_image_bkup.sh ${instance-id} ${host-name}
done
```
